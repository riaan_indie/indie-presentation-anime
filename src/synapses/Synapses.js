import React, { Component } from 'react';
import anime from 'animejs';
import './Synapses.scss';

export class Synapses extends Component {
  numberOfParticles = 60;
  primaryColor = '#F05A7D';
  animeClear = null;
  animeParticles = null;
  endCircleRadius = 7;
  animInProgress = false;
  animeInnerCircleIn = null;
  animeInnerCircleGrow = null;
  animeInnerCircleOut = null;
  fadeOutTimeout = null;
  circleGrowTimeout = null;

  constructor() {
    super();
  }

  componentDidMount() {
    this.createCanvas();
    this.animeClear = anime({
      duration: Infinity,
      autoplay: false,
      update: () => {
        this.clearCanvas();
      }
    });

    this.refs.synapsesContainer.addEventListener('click', e => this.onClick(e));

    this.start();
  }

  createCanvas() {
    const canvasEl = this.refs.synapses;
    canvasEl.width = 285;
    canvasEl.height = 252;
  }

  clearCanvas() {
    if (typeof this.refs.synapses === 'undefined') {
      return;
    }
    const canvasEl = this.refs.synapses;
    const context = canvasEl.getContext('2d');
    context.clearRect(0, 0, canvasEl.width, canvasEl.height);
  }

  onClick(e) {
    this.animateParticles(e.layerX, e.layerY);
  }

  autoAnimateParticles() {
    const point = this.getRandomPoint();
    this.animateParticles(point.x, point.y);
  }

  animateParticles(x, y) {
    if (this.animInProgress) {
      return;
    }

    this.animInProgress = true;

    if (this.animeClear) {
      this.animeClear.play();
    }

    let particles = [];
    for (let i = 0; i < this.numberOfParticles; i++) {
      particles.push(this.createParticle(x, y));
    }

    this.animeParticles = anime({
      targets: particles,
      value: 0,
      duration: 800,
      x: function(p) { return p.endPos.x; },
      y: function(p) { return p.endPos.y; },
      easing: 'easeInCubic',
      delay: anime.stagger(10),
      update: (anim) => this.renderParticles(anim)
    });

    let innerCircle = {
      x: x,
      y: y,
      radius: 6
    };

    this.circleGrowTimeout = setTimeout(() => {
      this.animeInnerCircleGrow = anime({
        targets: innerCircle,
        value: 0,
        radius: this.endCircleRadius,
        duration: 60,
        loop: 7,
        direction: 'alternate',
        easing: 'easeInCubic',
        update: (anim) => this.drawInnerCircle(anim),
        complete: (anim) => this.startInnerCircleAnim(anim)
      });
    }, 800);
  }

  createParticle(x,y) {
    var p = {};
    p.endPos = {
      x: x,
      y: y
    };
    var pOrigin = this.setParticleOrigin(p);
    p.x = pOrigin.x;
    p.y = pOrigin.y;
    p.color = this.primaryColor;
    p.radius = anime.random(3, 6);
    p.alpha = 0;

    const canvasEl = this.refs.synapses;

    if (typeof canvasEl === 'undefined') {
      return;
    }

    const context = canvasEl.getContext('2d');

    p.draw = () => {
      context.globalAlpha = p.alpha;
      context.fillStyle = p.color;
      context.beginPath();
      context.arc(p.x, p.y, p.radius, 0, 2 * Math.PI);
      context.fill();
      context.globalAlpha = 1;
    }
    return p;
  }

  setParticleOrigin(p) {
    const angle = anime.random(0, 360) * Math.PI / 180;
    const distance = anime.random(80, 200);
    const radius = [-1, 1][anime.random(0, 1)] * distance;

    return {
      x: p.endPos.x + radius * Math.cos(angle),
      y: p.endPos.y + radius * Math.sin(angle),
    }
  }

  renderParticles(anim) {
    let p;
    const radiusIncrease = anim.duration / 100000;
    let alphaIncrease = anim.duration / 130000;
    if (anim.progress < 24) {
      alphaIncrease = 0;
    }

    const canvasEl = this.refs.synapses;
    const context = canvasEl.getContext('2d');

    context.save();
    this.drawBlobberMask();

    for (var i = 0; i < anim.animatables.length; i++) {
      p = anim.animatables[i].target;
      p.radius -= radiusIncrease;
      p.alpha += alphaIncrease;
      p.draw();
    }

    context.restore();
  }

  drawBlobberMask() {
    const firstPoint = this.props.brainFillPoints[0];

    const canvasEl = this.refs.synapses;
    const context = canvasEl.getContext('2d');

    context.beginPath();
    context.moveTo(firstPoint.x, firstPoint.y);

    for (let i = 0; i < this.props.brainFillPoints.length; i++) {
      let endIndex = i + 1;
      if (endIndex >= this.props.brainFillPoints.length) {
        endIndex = 0;
      }
      let endPoint = this.props.brainFillPoints[endIndex];
      const anchorA = this.getAnchorControlPoint(i, 1);
      const anchorB = this.getAnchorControlPoint(endIndex, 0);
      context.bezierCurveTo(
        anchorA.x, anchorA.y,
        anchorB.x, anchorB.y,
        endPoint.x, endPoint.y
      );
    }
    context.closePath();
    context.clip();
  }

  startInnerCircleAnim(anim) {
    let innerCircle = {
      x: anim.animatables[0].target.x,
      y: anim.animatables[0].target.y,
      radius: 3
    };

    this.animeInnerCircleIn = anime({
      targets: innerCircle,
      value: 0,
      radius: this.endCircleRadius,
      duration: 400,
      easing: 'easeOutCubic',
      update: (anim) => this.drawInnerCircle(anim),
      complete: (anim) => this.startFadeOutAnim(anim)
    });
  }

  drawInnerCircle(anim) {
    const canvasEl = this.refs.synapses;

    if (typeof canvasEl === 'undefined') {
      return;
    }

    const context = canvasEl.getContext('2d');

    context.save();
    this.drawBlobberMask();

    context.fillStyle = this.primaryColor;
    context.beginPath();
    context.arc(anim.animatables[0].target.x, anim.animatables[0].target.y, anim.animatables[0].target.radius, 0, 2 * Math.PI);
    context.fill();

    context.restore();
  }

  startFadeOutAnim(anim) {
    this.animeClear.pause();

    let innerCircle = {
      x: anim.animatables[0].target.x,
      y: anim.animatables[0].target.y,
      radius: this.endCircleRadius
    };

    this.fadeOutTimeout = setTimeout(() => {
      this.animeClear.restart();

      this.animeInnerCircleOut = anime({
        targets: innerCircle,
        value: 0,
        radius: 0,
        duration: 600,
        easing: 'easeInCubic',
        update: (anim) => this.drawInnerCircle(anim),
        complete: () => this.completeInnerCircleAnim()
      });
    }, 800);
  }

  // get the control point on side A or B of the anchor
  getAnchorControlPoint(index, side) {
    const point = this.props.brainFillPoints[index];
    const length = point['length' + (side === 0 ? 'A' : 'B')]; // lengthA or lengthB
    const angle = point.angle + (side === 0 ? 0 : Math.PI); // add 180 if 'B'
    return {
      x: point.x + Math.cos(angle) * length,
      y: point.y + Math.sin(angle) * length,
    };
  }

  completeInnerCircleAnim() {
    this.animeClear.pause();
    this.animInProgress = false;
    this.autoAnimateParticles();
  }

  getRandomPoint() {
    const offsetX = 135;
    const offsetY = 100;
    const radius = 87;
    const ellipseAmount = 1.47;

    const angle = Math.random() * 2 * Math.PI;
    const r = radius * Math.sqrt(Math.random());

    return {
      x: offsetX + r * Math.sin(angle) * ellipseAmount,
      y: offsetY + r * Math.cos(angle)
    };
  }

  start() {
    if (!this.startWasCalled) {
      this.autoAnimateParticles();
      this.startWasCalled = true;
    }
  }

  stop() {
    if (this.startWasCalled) {
      this.startWasCalled = false;
      this.animInProgress = false;
      clearTimeout(this.fadeOutTimeout);
      clearTimeout(this.circleGrowTimeout);

      if (this.animeParticles) {
        this.animeParticles.pause();
      }
      if (this.animeInnerCircleIn) {
        this.animeInnerCircleIn.pause();
      }
      if (this.animeInnerCircleGrow) {
        this.animeInnerCircleGrow.pause();
      }
      if (this.animeInnerCircleOut) {
        this.animeInnerCircleOut.pause()
      }
      if (this.animeClear) {
        this.animeClear.pause();
      }

      this.clearCanvas();
    }
  }

  render() {
    return (
      <div className="SynapsesCanvas" ref="synapsesContainer">
        <canvas ref="synapses"></canvas>
      </div>
    )
  }
}
