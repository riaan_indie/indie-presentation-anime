import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import { resolveAnchor } from './utils/math';
import './App.scss';

import Growth from './growth/Growth';
import { WobblyController } from './wobbly-controller/WobblyController';
import { Synapses } from './synapses/Synapses';
import { Slide } from './slide/Slide';
import Simple from './simple/Simple';

export class App extends Component {
  brainFillPoints = [
	  {
	    control1: { x: 217.9, y: 12.62 },
	    control2: {x: 128.9, y: -9.21},
	    x: 173.4,
	    y: 1.7,
	  },
	  {
	    control1: { x: 292.18, y: 178.95 },
	    control2: { x: 310.55, y: 83.12 },
	    x: 301.36,
	    y: 131.04,
	  },
	  {
	    control1: { x: 194.42, y: 222.01 },
	    control2: { x: 254.75, y: 189.26 },
	    x: 225.08,
	    y: 205.64,
	  },
	  {
	    control1: { x: 163.57, y: 243.38 },
	    control2: { x: 217.25, y: 244.59 },
	    x: 190.41,
	    y: 243.99,
	  },
	  {
	    control1: { x: 68.28, y: 183.2 },
	    control2: { x: 144.56, y: 189.26 },
	    x: 106.42,
	    y: 186.23,
	  },
	  {
	    control1: { x: 49.7, y: 35.33 },
	    control2: {x: -60.98, y: 139.53 },
	    x: 34.37,
	    y: 49.76,
	  },
  ].map(resolveAnchor);

  wobblyController = null;

  componentDidMount() {
    this.createWobblyLines();
  }

  createWobblyLines({ containerEl = null, contentEl = null } = {}) {
    this.wobblyController = new WobblyController({
      containerEl: containerEl || this.refs.wobblyLines,
      contentEl: contentEl || this.refs.content
    });
  }

  render() {
    return (
      <div className="App">
        <content className="App-content" ref="content">
          <div className="Page-wobbles" ref="wobblyLines"></div>

          <Slide color="0x01011d">
            <div className="simple-container">
              <Simple />
            </div>
          </Slide>

          <Slide color="0x000034">
            <div className="growth-container">
              <Growth></Growth>
            </div>
          </Slide>

          <Slide color="0x00194D">
            <div className="brain-container">
              <div className="Brain">
                <Synapses brainFillPoints={this.brainFillPoints} ref="synapses" isInView={true} />
                <div className="Brain-outline">
                  <ReactSVG src="./assets/brain-placeholder.svg" />
                </div>
              </div>
            </div>
          </Slide>

        </content>
      </div>
    )
  }
}
