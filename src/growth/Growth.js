import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import anime from 'animejs';
import './Growth.scss';

export default class Growth extends Component {
  animeIntro = null;
  animeIntroLeaves = null;
  animeLeavesSway = null;
  animeBranchSway = null;
  hasStarted = false;

  start() {
    if (!this.hasStarted) {
      this.startIntroAnim();
    }
  }

  componentDidMount() {
    // this.start();
  }

  startIntroAnim() {
    if (typeof this.refs.stem === 'undefined') {
      return;
    }

    this.hasStarted = true;

    this.animeIntro = anime({
      targets: this.refs.stem.svgWrapper,
      easing: 'easeOutBack',
      scale: [0, 1],
      duration: 500,
      complete: () => this.startIntroLeaves()
    });
  }

  startIntroLeaves() {
    if (typeof this.refs.branch === 'undefined') {
      return;
    }

    this.animeIntroLeaves = anime({
      targets: this.refs.branch.getElementsByClassName('Page-image-growth__leaf'),
      easing: 'easeOutBack',
      scale: [0, 1],
      duration: 500,
      delay: anime.stagger(140, {direction: 'reverse'}),
      complete: () => this.startLeavesSway()
    });

    setTimeout(() => this.startBranchSwayAnim(), 500);
  }

  startLeavesSway() {
    if (typeof this.refs.branch === 'undefined') {
      return;
    }

    this.animeLeavesSway = anime({
      targets: this.refs.branch.getElementsByClassName('Page-image-growth__leaf'),
      loop: true,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: anime.random(0, 50),
      rotate: {
        value: anime.random(10, 12),
        duration: anime.random(1500, 1900),
        easing: 'easeInOutSine'
      }
    });
  }

  startBranchSwayAnim() {
    if (typeof this.refs.branch === 'undefined') {
      return;
    }

    this.animeBranchSway = anime({
      targets: this.refs.branch,
      loop: true,
      direction: 'alternate',
      easing: 'easeInOutSine',
      rotate: {
        value: [-4, 4],
        duration: 2000,
        easing: 'easeInOutSine'
      }
    });
  }

  render() {
    return (
      <>
        <div className="growth" ref="growth">
          <ReactSVG src="./assets/brain-growth-head.svg" className="Page-image-growth__head" ref="head" />
          <ReactSVG src="./assets/brain-growth-circle.svg" className="Page-image-growth__circle" ref="circle" />
          <div className="Page-image-growth__branch" ref="branch">
            <ReactSVG src="./assets/brain-growth-stem.svg" className="Page-image-growth__stem" ref="stem" />
            <ReactSVG src="./assets/brain-growth-head-leaf-1.svg" className="Page-image-growth__leaf-1 Page-image-growth__leaf" ref="leaf1" />
            <ReactSVG src="./assets/brain-growth-head-leaf-2.svg" className="Page-image-growth__leaf-2 Page-image-growth__leaf" ref="leaf2" />
            <ReactSVG src="./assets/brain-growth-head-leaf-3.svg" className="Page-image-growth__leaf-3 Page-image-growth__leaf" ref="leaf3" />
            <ReactSVG src="./assets/brain-growth-head-leaf-4.svg" className="Page-image-growth__leaf-4 Page-image-growth__leaf" ref="leaf4" />
            <ReactSVG src="./assets/brain-growth-head-leaf-5.svg" className="Page-image-growth__leaf-5 Page-image-growth__leaf" ref="leaf5" />
          </div>
        </div>

        <button className="button" onClick={() => this.start()}>play</button>
      </>
    )
  }
}
