import React, { Component } from 'react';
import SlideContainer from './Slide.scss'

export const SLIDE_COLOR_DARK = 0x2D2323;
export const SLIDE_COLOR_MEDIUM = 0x342A2A;

export function wobblyLineProps ({ color }) {
  return { color, 'data-color': color };
}

export class Slide extends Component {

  render() {
    return (
      <div className="SlideContainer" { ...wobblyLineProps({ color: this.props.color }) }>
        <div className="Page-slide">
          {this.props.children}
        </div>
      </div>
    )
  }
}
