import * as pixi from 'pixi.js';
import './WobblyController.scss';
import { WobblyLine } from '../wobbly-line/WobblyLine';

export class WobblyController {
  app = null;
  containerEl = null;
  contentEl = null;
  mouseX = null;
  mouseY = null;
  pages = [];
  scrollPosition = 0;
  overflowHeight = 30;
  shapeOffsetX = 0;
  wobblyLine = null;
  shapeHeight = 140;

  constructor({ containerEl, contentEl }) {
    this.containerEl = containerEl;
    this.contentEl = contentEl;
    this.app = new pixi.Application({
      width: 800,
      height: 800,
      antialias: true,
      transparent: true,
      resolution: 1,
    });

    this.containerEl.appendChild(this.app.view);

    let previousColor = 0x0;
    for (const page of contentEl.children) {
      // if no color is available, assume it's ignored
      if (page.dataset.color == null) continue;
      if (page.dataset.color === '') // repeat color of last one
        page.dataset.color = previousColor;
      this.pages.push({ page, line: null });
      previousColor = page.dataset.color;
    }

    this.createLine();
    this.updateLine();

    window.addEventListener('resize', this.resize);
    window.addEventListener('scroll', this.onScroll);
    window.addEventListener('mousemove', this.onMouseMove);

    this.resize();
  }

  createLine() {
    for (const item of this.pages) {
      const itemGraphics = new pixi.Graphics();
      this.app.stage.addChild(itemGraphics);

      const wobblyLine = new WobblyLine({app: this.app, color: item.page.dataset.color, graphics: itemGraphics });
      item.line = wobblyLine;
    }
  }

  updateLine() {
    for (const item of this.pages) {
      const { line, page } = item;
      line.update(this.shapeOffsetX, page.offsetTop - this.scrollPosition, page.offsetWidth, page.offsetHeight + this.overflowHeight * 1.5, this.overflowHeight);
    }
  }

  onScroll = (e) => {
    this.scrollPosition = window.scrollY;
    this.updateLine();
  }

  onMouseMove = (e) => {
    this.mouseX = e.clientX;
    this.mouseY = e.clientY;

    for (const item of this.pages) {
      item.line.onMouseMove({mouseX: this.mouseX, mouseY: this.mouseY});
    }
  }

  resize = () => {
    this.app.renderer.resize(this.containerEl.offsetWidth, this.containerEl.offsetHeight);
    this.updateLine();
  }

  destroy() {
    this.app.destroy();

    window.removeEventListener('resize', this.resize);
    window.removeEventListener('scroll', this.onScroll);
    window.removeEventListener('mousemove', this.onMouseMove);
  }
}
