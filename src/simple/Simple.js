import React, { Component } from 'react';
import anime from 'animejs';
import './Simple.scss';
import '../App.scss';

export default class Simple extends Component {
  animeIntro = null;
  tl = null;

  componentDidMount() {
    this.createTimeline();
    this.start();
  }

  createTimeline() {
    this.tl = anime.timeline({
      easing: 'easeOutExpo',
      duration: 750,
      autoplay: false
    });

    this.tl
    .add({
      targets: this.refs.block3,
      translateX: 350,
    })
    .add({
      targets: this.refs.block4,
      backgroundColor: '#772d58',
      translateX: 450,
    })
    .add({
      targets: this.refs.block5,
      translateX: 380,
    });
  }

  start() {
    anime({
      targets: this.refs.block1,
      left: [0, '70%'],
      backgroundColor: '#FFF',
      borderRadius: ['0%', '50%'],
      easing: 'easeInOutQuad'
    });

    anime({
      targets: this.refs.block2,
      left: {
        value: [0, '90%'],
        duration: 1100
      },
      rotate: {
        value: [0, 360],
        duration: 1800,
        easing: 'easeInOutSine'
      },
      scale: {
        value: [.8, 1],
        duration: 1600,
        delay: 800,
        easing: 'easeInOutQuart'
      },
      delay: 250 // All properties except 'scale' inherit 250ms delay
    });

    this.tl.play();

  }

  play() {
    this.start();
  }

  render() {
    return (
      <>
        <div className="track">
          <div className="block" ref="block1"></div>
        </div>

        <div className="track">
          <div className="block" ref="block2"></div>
        </div>

        <div className="track">
          <div className="block block-sm" ref="block3"></div>
          <div className="block block-sm" ref="block4"></div>
          <div className="block block-sm" ref="block5"></div>
        </div>

        <button className="button" onClick={() => this.play()}>repeat</button>
      </>
    )
  }
}
